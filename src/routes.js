import { createRouter, createMemoryHistory } from 'vue-router'
import StartView from './views/StartView.vue'
import RecView from './views/RecView.vue'

const router = createRouter({
  history: createMemoryHistory(),
  routes: [
    {
      path: '/start',
      component: StartView
    },
    {
      path: '/rec',
      component: RecView
    }
  ]
})

export default router
