
function getUser() {
	const url = new URL( window.location );
	let user = url.searchParams.get('user');
	if ( !user ) {
		user = crypto.randomUUID().replace(/-/g, '');
		url.searchParams.append('user', user );
		window.location = url.toString();
	}
	return user;
}

function getLang() {
	const SUPPORTED_LANGUAGES = [ 'en', 'es' ];
	const DEFAULT_LANGUAGE = SUPPORTED_LANGUAGES[0];
	const url = new URL( window.location );
	let lang = url.searchParams.get('lang');
	if ( !lang ) {
		return DEFAULT_LANGUAGE;
	}
	if ( SUPPORTED_LANGUAGES.indexOf( lang ) === -1 ) {
		return DEFAULT_LANGUAGE;
	}
	return lang;
}

export { getUser, getLang };
