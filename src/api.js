function recordVotes( user, lang, titles, votes ) {
	return fetch( '/votes', {
		method: 'post',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify( { user, lang, titles, votes } )
	} );
}

function getRecs( user, lang ) {
	return fetch( '/recs?user=' + user + '&lang=' + lang )
		.then( r => r.json() )
		.then( recs => Promise.all( recs.map( rec => getSummary( rec, lang ) ) ) );
}

function getSummary( title, lang ) {
	const url = 'https://' + lang + '.wikipedia.org/api/rest_v1/page/summary/' + encodeURIComponent( title );
	return fetch( url )
		.then( r => r.json() )
		.then( json => {
			return {
				title: json.title,
				description: json.description,
				thumbnail: json.thumbnail ? {
					url: json.thumbnail.source,
					width: json.thumbnail.width,
					height: json.thumbnail.height
				} : undefined,
				extract: json.extract,
				feel: 0
			};
		} );
}

export { recordVotes, getRecs };
