import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useWSRStore = defineStore('WSR', () => {
	const startTitle = ref( '' )
	function setStartTitle( title ) {
		startTitle.value = title
	}
	return {
		startTitle,
		setStartTitle,
	}
})
