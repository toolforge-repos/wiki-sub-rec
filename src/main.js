import { createApp } from 'vue'
import '../node_modules/@wikimedia/codex/dist/codex.style.css'
import App from './App.vue'
import routes from './routes.js'
import { getUser } from './util.js';

// just calling it here to make sure
// the user is populated in the url
getUser();

createApp(App)
	.use(routes)
	.mount('#app')
routes.replace('/start')
