import db from './db.js';

const N_LEVELS = 3;
const N_PER_LEVEL = 5;
const FADE = 0.8;

async function scoreAll( user, lang, titles, votes ) {
  const scores = { value: {} };

  if ( titles.length ) {
    titles = toDbKeys( titles );
    await scoreTitles( user, lang, titles, 1, 2, scores );
  }

  const upVotes = toDbKeys( getVotes( votes, 1 ) );
  if ( upVotes.length ) {
    await scoreTitles( user, lang, upVotes, 1, 1, scores );
    await db.query(
      'update recs set vote=1 where user=? and title in (?);',
      [ user, upVotes ]
    );
  }
  const downVotes = toDbKeys( getVotes( votes, -1 ) );
  if ( downVotes.length ) {
    await scoreTitles( user, lang, downVotes, 1, 1, scores );
    await db.query(
      'update recs set vote=-1 where user=? and lang=? and title in (?);',
      [ user, lang, downVotes ]
    );
  }

  if ( Object.keys( scores.value ).length ) {
    const consideredTitles = [];
    consideredTitles.push( ...titles );
    consideredTitles.push( ...upVotes );
    consideredTitles.push( ...downVotes );
    const dataToInsert = Object.entries( scores.value ).map( e => {
      const given = consideredTitles.indexOf( e[0] ) !== -1;
      return [ user, lang, e[0], e[1], given ];
    } );
    await db.query( `
      insert into recs (user, lang, title, value, given)
      values ?
      on duplicate key update
      value = value + value(value),
      given = given or value(given);`,
      [ dataToInsert ]
    );
  }
}

async function findRecs( user, lang, n ) {
  const [ recFromDb ] = await db.query( `
    select title from recs
    where user = ?
    and lang = ?
    and given = false
    order by value desc
    limit ?;
  `, [ user, lang, n * 2 ] );
  const actualRecs = recFromDb.map( r => r.title );
  shuffleArray( actualRecs );
  const selectedRects = actualRecs.slice( 0, n );

  if ( selectedRects.length > 0 ) {
    await db.query( `
      update recs
      set given = true
      where user = ?
      and lang = ?
      and title in (?);`,
      [ user, lang, selectedRects ]
    );
  }

  return selectedRects;
}

async function scoreTitles( user, lang, titlesToScore, level, initialValue, scores ) {
  const titles = await getNextLevelTitles( user, lang, titlesToScore, N_PER_LEVEL );
  const value = initialValue * ( level * FADE );
  for ( let i=0; i<titles.length; i++ ) {
    scores.value[ titles[i] ] = ( scores.value[ titles[i] ] || 0 ) + value;
  }
  if ( level < N_LEVELS && titles.length ) {
    await scoreTitles( user, lang, titles, level + 1, initialValue, scores );
  }
}

async function getNextLevelTitles( user, lang, titles, limit ) {
  const queries = [];
  const params = [];
  for (var i = 0; i < titles.length; i++) {
    queries.push( `
      (select curr from clickstream
      where prev = ?
      and lang = ?
      and curr not like 'List_of%'
      and curr not like 'Lists_of%'
      and curr not like '%(disambiguation)'
      and curr not in (
        select title curr
        from recs
        where user = ?
        and given = true
      )
      order by n desc limit ?)
    ` );
    params.push( titles[i] );
    params.push( lang );
    params.push( user );
    params.push( limit );
  }
  const [ nextTitles ] = await db.execute(
    queries.join( ' union ' ) + ';',
    params );
  return nextTitles.map( t => t.curr );
}

function getVotes( votes, value ) {
  return Object.keys( votes )
    .filter( r => votes[r] === value );
}

function toDbKeys( titles ) {
  return titles.map( t => t.replace( /\s/g, '_' ) );
}

function shuffleArray( array ) {
    for ( let i = array.length - 1; i > 0; i-- ) {
        const j = Math.floor( Math.random() * ( i + 1 ) );
        [ array[i], array[j] ] = [ array[j], array[i] ];
    }
}

export { scoreAll, findRecs };
