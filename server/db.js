import mysql  from 'mysql2/promise';
import PropertiesReader  from 'properties-reader';

const properties = PropertiesReader( process.argv[2] );
const host = properties.get('client.host') || 'tools.db.svc.wikimedia.cloud';
const user = properties.get('client.user');
const password = properties.get('client.password');
const db = properties.get('client.db') || user + '__wikisubrec';

const pool = await mysql.createPool({
    host,
    port: 3306,
    database: db,
    user,
    password
  });

export default pool;
