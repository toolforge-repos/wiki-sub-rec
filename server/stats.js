import db from './db.js';

async function getStats( user ) {
	const results = await db.query( `
		select
			count(*) as total,
			sum(case when vote=1 then 1 else 0 end) up_votes,
			sum(case when vote=-1 then 1 else 0 end) down_votes,
			sum(vote) score,
			sum(given) seen_recs
		from recs
		where user=?
		group by user;
	`, [ user ] );
	return results[0][0];
}

export { getStats };
