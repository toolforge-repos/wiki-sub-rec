import express from 'express';
import cors from 'cors';
import { scoreAll, findRecs } from './recs.js';

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(cors());
app.use(express.static("dist"));

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

/**
 * Record users article preferences
 **/
app.post( '/votes', async ( req, res ) => {
  const { user, lang, titles, votes } = req.body;
  if ( !user ) {
    return res.status(400)
      .send('Missing required user parameter');
  }
  if ( !lang ) {
    return res.status(400)
      .send('Missing required lang parameter');
  }
  await scoreAll( user, lang, titles, votes );
  return res.send('OK');
} );

app.get('/recs', async ( req, res ) => {
  const { user, lang, n=4 } = req.query;
  if ( !user ) {
    return res.status(400)
      .send('Missing required user parameter');
  }
  if ( !lang ) {
    return res.status(400)
      .send('Missing required lang parameter');
  }
  const result = await findRecs( user, lang, n );
  return res.json( result );
} );
