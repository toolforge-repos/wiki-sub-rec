create table clickstream
(
	prev nvarchar(255) not null,
	curr nvarchar(255) not null,
	type nvarchar(10) not null,
	n integer not null,
	lang nvarchar(10) not null
);
create index clickstream_lang_prev on clickstream (lang, prev);

create table recs
(
	user nvarchar(255),
	title nvarchar(255),
	value double,
	given bool,
	vote integer,
	lang nvarchar(10) not null,
	primary key(user, title)
);
